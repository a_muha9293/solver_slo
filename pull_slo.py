import xlwings as xw
from pathlib import Path


# Path objects
solv_path = Path.home() / 'Code' / 'py_in_the_sky' / 'work' / 'solver_SLO'
try:
        slo_path = next(
                x for x in solv_path.glob('*.xlsx')
                  if x.is_file() and '~' not in str(x)
        )
except StopIteration:
        raise Exception("Add one OEM_Model.xlsx file to begin.")

# Book and Sheet objects
solv = xw.Book(str(solv_path / 'solver_SLO.xlsm'))
calc = solv.sheets['SOLVER']

slo = xw.Book(str(slo_path))

slon = slo.sheets(
        next(s.name for s in slo.sheets
    if 'SLO' in s.name and 'TEMPLATE' in s.name
    or 'SLON' in s.name)
)

slon.api.Unprotect("LDV")
slon.api.Unprotect()
