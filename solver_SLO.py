import xlwings as xw
import datetime as dt
from pathlib import Path
from openpyxl.utils import (
    get_column_letter as gcl,
    column_index_from_string as gcn,
)

from pull_slo import (
    # sheet variables
    solv,
    calc,
    slo,
    slon,
)
from utils import (
    delete_results,
    find_model_address,
    concat_pwt,
    TableOutOfRangeError,
)

@xw.sub
def main():
    wb = xw.Book.caller()
    wb.sheets['SOLVER'].range("A1").value = "Hello xlwings!"

@xw.sub
def pull_slo():
    """Clears SLOCOPY sheet and creates a new one based on 
    the one .xlsx file in the folder.
    """
    solv.app.display_alerts = False
    solv.app.screen_updating = False
    
    # Remove all sheets from calculator except the calculator
    for s in solv.sheets:
        if s.name != 'SOLVER':
            solv.sheets(s).delete()

    # add sheet
    solv.sheets.add(
        slon['F100'].value + ' SLOCOPY'
    )

    # define new sheet object
    copy = solv.sheets(next(s.name for s in solv.sheets
            if 'SLOCOPY' in s.name))


    # build values for validation list
    copy['A1'].options(transpose=True).value = (
        tuple(set(slon['B100:B909'].options(numbers=int).value))
    )

    # setting up log
    copy['B1'].options(transpose=True).value = tuple(range(14, 31))
    copy.api.Columns("A:B").Hidden = True
    copy['C1'].value = 'LOG'
    copy['E1'].value = 'ERRORS'
    copy['C:D'].column_width = 25
    copy['E:E'].column_width = 60
    copy.api.Tab.ColorIndex = 7

    # verification
    scen = slo.sheets['Scenario']
    if scen['A1'].value != copy.name.split()[0]:
        copy['E100000'].end('up').offset(1).value = ([
                'Scenario A1 value "' + str(scen['A1'].value)
                + '" does not match pulled SLO F100 value "'
                + copy.name.split()[0] + '"',
                '@ ' + str(dt.datetime.now()),
        ])
        raise Exception('SLO and Scenario don\'t match')

    calc.activate()
    delete_results()
    solv.app.display_alerts = True
    solv.app.screen_updating = True

@xw.sub
def validation():
    """Clears out old validation list and creates a new one."""
    solv.app.display_alerts = False
    solv.app.screen_updating = False

    copy = solv.sheets(next(s.name for s in solv.sheets
            if 'SLOCOPY' in s.name))

    name_rng = "=\'" + copy.name + "\'!"
    name_rng += copy['A1:A' + str(copy['A1'].end('down').row)].address
    year_rng = "=\'" + copy.name + "\'!"
    year_rng += copy['B1:B' + str(copy['B1'].end('down').row)].address

    # clear old list
    calc['g12:h13'].clear()

    # add new lists
    calc['G13'].api.Validation.Add(3, 3, 3, name_rng)
    calc['H13'].api.Validation.Add(3, 3, 3, year_rng)

    calc['G12'].value = 'Select Nameplate'
    calc['H12'].value = 'Select Model Year'
    calc['G12:H13'].api.Font.Size = 14

    calc['G12:H13'].api.HorizontalAlignment = -4108
    calc['G12:H13'].api.Borders.LineStyle = 1
    calc['G12:H12'].api.Font.Bold = True

    calc['G12:H12'].color = (217, 217, 217)
    calc['G13:H13'].color = (214, 220, 228)

    solv.app.display_alerts = True
    solv.app.screen_updating = True

@xw.sub
def import_data():
    """Will import data based on nameplate and model year selected."""

    g13 = str(calc['G13'].value)
    h13 = calc['H13'].options(numbers=int).value

    assert g13 != None, 'Please select a nameplate'
    assert h13 != None, 'Please select a model year'

    slon['A99'].api.AutoFilter(Field=2, Criteria1=g13)

    # this is pulling values whose locations are hardcoded in
    # this has the potential to break if the SLO changes
    for cell in slon['B100:B909']:
        if cell.value == g13:
            row_start = str(cell.row)
            row_end = str(cell.offset(29, 0).row)
            vol = gcl(slon['A1'].offset(0, h13).column)
            fe = gcl(slon['A1'].offset(0, h13 + 17).column)
            name_col = slon['B' + row_start +':B' + row_end].value
            pwt_col = concat_pwt(row_start, row_end)
            vol_col = slon[vol + row_start +':' + vol + row_end].value
            fe_col = slon[fe + row_start + ':' + fe + row_end].value
            break

    calc.activate()
    calc['G16'].options(transpose=True).value = name_col
    calc['H16'].options(transpose=True).value = pwt_col
    calc['K16'].options(transpose=True).value = vol_col
    calc['J16'].options(transpose=True).value = fe_col
    calc['H12:H45'].autofit()

    calc['R15'].value = g13 + ' MY' + str(h13)
    calc['R15'].autofit()
    calc['K47'].value = calc['K46'].value


@xw.sub
def save_data():
    calc['R:R'].api.Copy()
    calc['S:S'].api.Insert()
    calc['S:S'].api.PasteSpecial(-4163)
    calc['S14'].number_format = 'mm/dd/yyyy hh:mm:ss'
    calc['S14'].value = dt.datetime.now()
    calc['S14'].api.Font.Size = 12
    calc['S14:S15'].autofit()
    calc['S10'].value = None


@xw.sub
def export_data():
    scen = slo.sheets('Scenario')
    sel = calc.book.app.selection

    # split on range address to get two cell address 
    # then split again to get column
    table_col = (
        calc['R14'].expand('table')
            .address
            .split(':')[1]
            .split('$')[1]
    )
    
    assert sel.sheet.book.name == 'solver_SLO.xlsm',\
    "Selection must be in solver_SLO.xlsm"
    
    assert sel.sheet.name == 'SOLVER', 'Selection must be in the SOLVER tab'
    
    assert sel.column in range(19, gcn(table_col) + 1),\
    'Selection must be a column within $S$16:$' + table_col + '$45'
    
    assert sel.row in range(16, 46),\
    'Selection must be a column within $S$16:$' + table_col + '$45'
    
    assert sel.columns.count == 1, 'Select only one column'
    assert sel.rows.count == 30, 'Must select 30 rows'
    
    model_row = find_model_address()
    # this year column is hardcoded in to be 8 removed from the model year
    # if more columns are added to the SLO this could break
    year_col = gcl(int(calc[gcl(sel.column) + '15'].value.split(' MY')[1]) - 8)
    year = calc['R15'].value.split(' MY')[1]
    
    # resets for the coming line
    sel.options(transpose=True).value = tuple(map(
        lambda x: 0 if x is None else x,
        sel.value
    ))

    # will overwrite saved data to clear cells that are 0
    # rounds all other values to one decimal place
    sel.options(transpose=True).value = tuple(map(
        lambda x: None if x == 0 else round(x, 2),
        sel.value,
    ))

    # export
    sel.api.Copy()
    paste_range = scen[year_col + str(model_row)].offset(2)
    paste_range.api.PasteSpecial(-4163)


    # log
    copy = solv.sheets(
        next(s.name for s in solv.sheets if 'SLOCOPY' in s.name)
    )
    copy['C100000'].end('up').offset(1).value = ([
        'Pasted in ' + paste_range.get_address(
            row_absolute=True,
            column_absolute=True,
            include_sheetname=True,
            external=False,
        ),
        '@ ' + str(dt.datetime.now()),
    ])
    


if __name__ == "__main__":
    xw.books.active.set_mock_caller()
