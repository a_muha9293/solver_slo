import xlwings as xw
import datetime as dt
from pathlib import Path
from openpyxl.utils import get_column_letter as gcl
from openpyxl.utils import column_index_from_string as gcn
from pull_slo import solv, calc, slo, slon


def find_model_address():
    """Will return the row address of the export-data's nameplate
    in the Model Scenario."""
    sel = calc.book.app.selection
    
    model = calc[gcl(sel.column) + '15'].value.split(' MY')[0]
    slo.sheets('Scenario').activate()
    start = xw.Range('B10')
    while(start.row < 1415):
        if start.value == model:
            break
        start = start.offset(54, 0)

    # this address is supposed to be the last 'model' in the scenario tab
    # if that changes the script can break
    assert start.row <= 1414, 'No ' + model + ' in ' + slo.name 
    return start.row


def delete_results():
    """Deletes all columns from Results table."""
    while(True):
        if calc['R14'].offset(0, 1).value == None:
            calc['R10'].value = (
                'Session started on ' 
                 + dt.datetime.now().strftime("%b %d %Y %H:%M:%S")
            )
            calc['R10'].api.HorizontalAlignment = 1
            calc['R10'].api.Font.Size = 14
            calc['R10'].api.Font.Bold = True
            break
        calc['R:R'].offset(0, 1).api.Delete()

def log_exception(exc: str):
    """Writes a string error to the log file and raises the exception.
    TODO: implement..."""
    copy = slo.sheets(next(s.name for s in slo.sheets if 'SLOCOPY' in s.name))
    copy['E100000'].end('up').offset(1).value = ([
        exc,
        '@ ' + str(dt.datetime.now()),
    ])

    raise Exception(exc)

def concat_pwt(row_start, row_end):
    """Concatenates powertrain values from SLO."""
    pwt = list(slon['N' + row_start + ':N' + row_end].value)
    disp = list(
        map(lambda x: clean_disp(x),
        slon['G' + row_start + ':G' + row_end].value)
        )
    asp = list(
        map(lambda x: '-T' if x == 'Yes' else '',
        slon['J' + row_start + ':J' + row_end].value)
        ) 
    dies = list(
        map(lambda x: 'D' if x == 'Yes' else '',
        slon['K' + row_start + ':K' + row_end].value)
        )
    ss = list(
        map(lambda x: ' S&S' if x == 'Yes' else '',
        slon['L' + row_start + ':L' + row_end].value)
        )
    coast = list(
        map(lambda x: ' + COAST' if x == 'Yes' else '',
        slon['M' + row_start + ':M' + row_end].value)
        )
    trans = list(
        map(lambda x: '; ' + str(x) if x != 0.0 else '',
        slon['H' + row_start + ':H' + row_end].value)
        )
    drive = list(
        map(lambda x: clean_drive(x),
        slon['I' + row_start + ':I' + row_end]
        .options(numbers=int).value)
        ) 
    power = list(zip(pwt, disp, asp, dies, ss, coast, trans, drive))
    
    return tuple(''.join(sublist) for sublist in power)

def clean_drive(x):
    '''Utility function for cleaning driveline data.'''
    x = str(x)
    if x == 'FW':
        return ' -- FWD'
    elif x == 'FF':
        return ' -- FWD'
    elif x == 'FR':
        return ' -- RWD'
    elif x == '0':
        return ''
    elif x == '-':
        return ''
    else:
        return ' -- ' + x

def clean_disp(x):
    '''Utility function for cleaning displacement data.'''
    x = str(x)
    if x == 'BEV':
        return ''
    elif x == 'FCV':
        return ''
    elif x == '':
        return ''
    else:
        return ' ' + x + 'L '

class TableOutOfRangeError(Exception):
    '''Throw an error if selection isn't valid for export.'''

    def __init__(self, table_col):
        '''Input a column name for the last column in the results table.'''
        self.table_col=table_col
        
    
    def __str__(self):
        msg = ('Selection must be a column within the following range:'
                + f' $S$16:${self.table_col}$45')
        return msg